﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //GenericList<string> list = new GenericList<string>();

            //list.AddList("lam sao");
            //list.AddList("ha");
            //list.AddList("lam sao vay");

            //list.RemoveList("lam sao");
            //list.ShowList();
            //list.InsertAt(1, "lala");
            //list.ShowList();

            //Console.WriteLine(list.CountList);

            GenericConstraint<A> const_Generic = new GenericConstraint<A>();
            B b = new B();
            const_Generic.ShowPropertyAndValue(b);


            Console.ReadLine();
        }
    }
    public class A { }
    public class B: A { }
}
