﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic
{
    public class GenericList<T>
    {
        
        private List<T> genericList = null;
        private int _countList;

        public int CountList
        {
            get { 
                _countList = genericList.Count;
                return _countList; 
            }  
        }

        public GenericList(){
            genericList = new List<T>();
        }

        public void AddList(T t) { 
            genericList.Add(t);
        }
        public void RemoveList(T t) {
            int  index  = genericList.IndexOf(t);
            genericList.Remove(genericList[index]);
        }

        public void ShowList() {
            foreach (T item in genericList) {
                Console.WriteLine(item);
            }
        }
        public int GetIndexof(T t) { 
            return genericList.IndexOf(t);
        }

        public void InsertAt(int index , T t) {
            genericList.Insert(index, t);
        }

    }
}
